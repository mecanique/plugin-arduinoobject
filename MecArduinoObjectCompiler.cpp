/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecArduinoObjectCompiler.h"

MecArduinoObjectCompiler::MecArduinoObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecObjectCompiler(Object, MainCompiler)
{
}

MecArduinoObjectCompiler::~MecArduinoObjectCompiler()
{
}
	
QList<QResource*> MecArduinoObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/src/Arduino/Arduino.h"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/README"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator.h"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport.pri"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator_win.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator_osx.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator_p.h"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/LICENSE"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator_linux.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialenumerator_unix.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport.h"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport_win.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport_unix.cpp"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport_p.h"));
tempList.append(new QResource(":/src/Arduino/QextSerialPort/qextserialport_global.h"));
tempList.append(new QResource(":/src/Arduino/Arduino.cpp"));
tempList.append(new QResource(":/share/icons/types/Arduino.png"));
tempList.append(new QResource(":/share/translations/Arduino.fr.qm"));

tempList.append(new QResource(":/src/mecanique/string.h"));
return tempList;
}

QString MecArduinoObjectCompiler::projectInstructions()
{
return QString("include(Arduino/QextSerialPort/qextserialport.pri)\nHEADERS += Arduino/Arduino.h mecanique/string.h\nSOURCES += Arduino/Arduino.cpp\n");
}
	
QString MecArduinoObjectCompiler::header()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2014 – 2015\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "#ifndef __" + object()->elementName().toUpper() + "_H__\n#define __" + object()->elementName().toUpper() + "_H__\n\n#include \"Arduino/Arduino.h\"\n";

for (int i=0 ; i < recConcreteSubCompilers().size() ; i++)
	{
	tempString += recConcreteSubCompilers().at(i)->preprocessorInstructions();
	}

tempString += "\nclass " + object()->elementName() + " : public Arduino\n{\nQ_OBJECT\n\tpublic:\n";
tempString += object()->elementName() + "(Project* const Project);\n~" + object()->elementName() + "();\n\nQString identifier() const;\n\n";

tempString += "\tpublic slots:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function
		and concreteSubCompilers().at(i)->element()->elementName() != "identifier"
		and concreteSubCompilers().at(i)->element()->elementName() != "isConnected") tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tsignals:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tprivate:\nvoid signalReceived(char Code);\n";

tempString += "};\n\n\n#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n";

return tempString;
}

QString MecArduinoObjectCompiler::source()
{
QString tempString;

//On établi la liste par ordre alphabétique des éléments enfants.
QList<MecAbstractElement*> tempListElements;
QStringList tempElementsNames;
for (int i=0 ; i < element()->childElements().size() ; i++)
	{
	if (element()->childElements().at(i)->elementName() != "identifier" and element()->childElements().at(i)->elementName() != "isConnected")
		tempElementsNames.append(element()->childElements().at(i)->elementName());
	}
tempElementsNames.sort();

for (int i=0 ; i < tempElementsNames.size() ; i++)
	{
	for (int j=0 ; j < element()->childElements().size() ; j++)
		{
		if (tempElementsNames.at(i) == element()->childElements().at(j)->elementName())
			tempListElements.append(element()->childElements().at(j));
		}
	}

//Et l'identifiant correspondant.
QString tempIdentifier;
QString typesString;
for (int i=0 ; i < tempListElements.size() ; i++)
	{
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		typesString += tempListElements.at(i)->childElements().at(j)->elementType();
		}
	}
tempIdentifier = element()->elementName() + QString(QCryptographicHash::hash(typesString.toUtf8(), QCryptographicHash::Md4).toHex());

tempString = "/*\n\
© Quentin VIGNAUD, 2014 – 2015\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "\n#include \"" + object()->elementName() + ".h\"\n\n";

tempString += object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : Arduino(\"" + object()->elementName() + "\", Project)\n{\n";

//Pas de variables.

tempString += "}\n\n" + object()->elementName() + "::~" + object()->elementName() + "()\n{\n}\n\nQString " + object()->elementName() + "::identifier() const\n{\n\treturn QString(\"" + tempIdentifier + "\");\n}\n\n";

//Fonctions
for (int i=0 ; i < tempListElements.size() ; i++)
	{
	if (tempListElements.at(i)->elementRole() == MecAbstractElement::Function)
		{
		MecAbstractElement *tempFunction = tempListElements.at(i);
		tempString += tempFunction->elementType() + " " + tempFunction->parentElement()->elementName() + "::" + tempFunction->elementName() + "(";
			
		for (int j=0 ; j < tempFunction->childElements().size() ; j++)
			{
			tempString += tempFunction->childElements().at(j)->elementType() + " " + tempFunction->childElements().at(j)->elementName();
			if (j != tempFunction->childElements().size() - 1) tempString += ", ";
			}
		
		tempString += ")\n{\n";
		
		tempString += "prepareFunction(0x" + QString::number(i, 16) + ");\n";
		for (int j=0 ; j < tempFunction->childElements().size() ; j++)
			{
			if (tempFunction->childElements().at(j)->elementType() == "bool") tempString += "prepareBool(" + tempFunction->childElements().at(j)->elementName() + ");\n";
			else if (tempFunction->childElements().at(j)->elementType() == "int") tempString += "prepareInt(" + tempFunction->childElements().at(j)->elementName() + ");\n";
			else if (tempFunction->childElements().at(j)->elementType() == "uint") tempString += "prepareUInt(" + tempFunction->childElements().at(j)->elementName() + ");\n";
			}
		
		tempString += "send();\n}\n\n";
		}
	}

//void signalReceived(char Code)
tempString += "void " + element()->elementName() + "::signalReceived(char Code)\n{\n";

//"signaux"
bool firstSignal = true;
for (int i=0 ; i < tempListElements.size() ; i++)
	{
	if (tempListElements.at(i)->elementRole() == MecAbstractElement::Signal)
		{
		//Compte du nombre d'octets necéssaires.
		int nbBytes = 1;//"1" pour le code de signal.
		for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
			{
			//Préparation de chaque variable.
				if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool") nbBytes += 1;
				else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int") nbBytes += 2;
				else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint") nbBytes += 2;
			}
		
		if (firstSignal) firstSignal = false;
		else tempString += "else ";
		
		tempString += "if (Code == 0x" + QString::number(i, 16) + " and serialPort->bytesAvailable() >= " + QString::number(nbBytes) + ")\n\t{\n\tserialPort->read(1);\n";
		for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
			{
			//Préparation de chaque variable.
			if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool") tempString += "\tbool variable" + QString::number(j) + " = takeBool();\n";
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int") tempString += "\tint variable" + QString::number(j) + " = takeInt();\n";
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint") tempString += "\tuint variable" + QString::number(j) + " = takeUInt();\n";
			}
		
		//Émission du signal.
		tempString += "\temit " + tempListElements.at(i)->elementName() + "(";
		//Variables-paramètres
		for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
			{
			tempString += "variable" + QString::number(j);
			if (j != tempListElements.at(i)->childElements().size() - 1) tempString += ", ";
			}
		tempString += ");\n\t}\n";
		}
	}

tempString += "}\n\n";

return tempString;
}


