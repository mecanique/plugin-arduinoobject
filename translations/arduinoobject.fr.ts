<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecArduinoFunctionEditor</name>
    <message>
        <location filename="../MecArduinoFunctionEditor.cpp" line="36"/>
        <source>New variable</source>
        <translation>Nouvelle variable</translation>
    </message>
    <message>
        <location filename="../MecArduinoFunctionEditor.cpp" line="36"/>
        <source>Type of the new variable:</source>
        <translation>Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../MecArduinoFunctionEditor.cpp" line="42"/>
        <source>Add the variable “%1”</source>
        <translation>Ajouter la variable « %1 »</translation>
    </message>
</context>
<context>
    <name>MecArduinoObjectEditor</name>
    <message>
        <location filename="../MecArduinoObjectEditor.cpp" line="24"/>
        <source>Copy Arduino code</source>
        <translation>Copier le code Arduino</translation>
    </message>
    <message>
        <location filename="../MecArduinoObjectEditor.cpp" line="96"/>
        <source>Add the variable “%1”</source>
        <translation>Ajouter la variable « %1 »</translation>
    </message>
    <message>
        <location filename="../MecArduinoObjectEditor.cpp" line="103"/>
        <source>Add the signal “%1”</source>
        <translation>Ajouter le signal « %1 »</translation>
    </message>
</context>
<context>
    <name>MecArduinoObjectPlugin</name>
    <message>
        <location filename="../MecArduinoObjectPlugin.cpp" line="32"/>
        <source>Design an Arduino object to include into a Mécanique project.</source>
        <translation>Concevoir un objet Arduino à intégrer dans un projet Mécanique.</translation>
    </message>
</context>
<context>
    <name>MecArduinoSignalEditor</name>
    <message>
        <location filename="../MecArduinoSignalEditor.cpp" line="33"/>
        <source>New variable</source>
        <translation>Nouvelle variable</translation>
    </message>
    <message>
        <location filename="../MecArduinoSignalEditor.cpp" line="33"/>
        <source>Type of the new variable:</source>
        <translation>Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../MecArduinoSignalEditor.cpp" line="39"/>
        <source>Add the variable “%1”</source>
        <translation>Ajouter la variable « %1 »</translation>
    </message>
</context>
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <source>New object</source>
        <translation type="obsolete">Nouvel objet</translation>
    </message>
    <message>
        <source>Type of the new object:</source>
        <translation type="obsolete">Type du nouvel objet :</translation>
    </message>
    <message>
        <source>New function</source>
        <translation type="obsolete">Nouvelle fonction</translation>
    </message>
    <message>
        <source>Return type of the new function:</source>
        <translation type="obsolete">Type de retour de la nouvelle fonction :</translation>
    </message>
    <message>
        <source>New signal</source>
        <translation type="obsolete">Nouveau signal</translation>
    </message>
    <message>
        <source>Type of the new signal:</source>
        <translation type="obsolete">Type du nouveau signal :</translation>
    </message>
    <message>
        <source>New variable</source>
        <translation type="obsolete">Nouvelle variable</translation>
    </message>
    <message>
        <source>Type of the new variable:</source>
        <translation type="obsolete">Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished">Ajouter le signal « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation type="unfinished">Ajouter la variable « %1 »</translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation>Type de retour :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation>Signaux</translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation>Synopsis</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation>Signaux</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation>Ajouter connexion</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation>Supprimer connexion</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation>Connexions</translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
