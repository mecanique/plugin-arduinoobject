<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Arduino</name>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="34"/>
        <source>No connection.</source>
        <translation>Aucune connexion.</translation>
    </message>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="53"/>
        <source>Choose a serial port...</source>
        <translation>Choisir un port série…</translation>
    </message>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="53"/>
        <source>Select the serial port which correspond to %1:</source>
        <translation>Sélection du port série correspondant à %1 :</translation>
    </message>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="167"/>
        <source>Connection to &quot;%1&quot; impossible.</source>
        <translation>Connexion à « %1 » impossible.</translation>
    </message>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="174"/>
        <source>Try connection to &quot;%1&quot;...</source>
        <translation>Tentative de connexion à « %1 »…</translation>
    </message>
    <message>
        <location filename="../../src/Arduino/Arduino.cpp" line="183"/>
        <source>Connected to &quot;%1&quot;.</source>
        <translation>Connecté à « %1 ».</translation>
    </message>
</context>
</TS>
