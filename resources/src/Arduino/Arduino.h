/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __ARDUINO_H__
#define __ARDUINO_H__

#include "../mecanique/Object.h"
#include "../mecanique/string.h"
#include "QextSerialPort/qextserialenumerator.h"
#include "QextSerialPort/qextserialport.h"
#include <QInputDialog>

/**
\brief	Classe d'interface avec une Arduino par port série.
*/
class Arduino : public Object
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Projet	Projet d'appartenance.
	*/
	Arduino(QString Name, Project* const Project);
	/**
	\brief	Destructeur.
	
	Déconnecte le port série.
	*/
	~Arduino();
	
	///Ouvre le sélecteur de port série.
	void more();
	
	///Retourne les paramètres de connexion.
	QVariant settings();
	///Charge les paramètres de connexion.
	void setSettings(QVariant Settings);
	
	///Retourne l'identifiant de l'objet Arduino.
	virtual QString identifier() const = 0;
	
	///Indique si la connexion est en cours.
	bool isConnected() const;
	
	protected:
	/**
	\brief	Fixe l'identificateur de fonction à envoyer à l'Arduino.
	*/
	void prepareFunction(char Code);
	/**
	\brief	Ajoute un booléen au flux d'envoi.
	*/
	void prepareBool(bool Value);
	/**
	\brief	Ajoute un entier au flux d'envoi.
	*/
	void prepareInt(int Value);
	/**
	\brief	Ajoute un entier non signé au flux d'envoi.
	*/
	void prepareUInt(unsigned int Value);
	/**
	\brief	Envoi le flux préparé.
	*/
	void send();
	
	///Récupère un booléen dans le flux de réception.
	bool takeBool();
	///Récupère un entier dans le flux de réception.
	int takeInt();
	///Récupère un entier non signé dans le flux de réception.
	unsigned int takeUInt();
	
	///Gère l'exécution des signaux reçus.
	virtual void signalReceived(char Code) = 0;
	
	///Port série de communication.
	QextSerialPort *serialPort;
	
	private:
	///Timer de délai de connexion.
	QTimer m_timer;
	///Flux à envoyer.
	QByteArray arrayToSend;
	///Indique si la connexion est ouverte.
	bool m_connected;
	/**
	\brief	Tente une connexion à une Arduino.
	\param	Name	Nom du port de connexion.
	*/
	void tryConnection(QString Name);
	/**
	\brief	Confirme la connexion.
	*/
	void connectionSuccess();
	
	private slots:
	///Envoi la requête de connexion à l'Arduino.
	void sendConnectSignal();
	/**
	\brief	Procédure de gestion de réception de fonction.
	
	Lit l'octet d'identifiant de fonction, sans le retirer du flux, et appelle « signalReceived() ».
	
	\note	Est également chargé de la confirmation de connexion, avec connectionSuccess().
	*/
	void readSerialFunction();
	
};

#endif /* __ARDUINO_H__ */

