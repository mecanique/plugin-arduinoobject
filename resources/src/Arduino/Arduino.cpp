/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Arduino.h"

Arduino::Arduino(QString Name, Project* const Project) : Object(Name, QString("Arduino"), Project)
{
	m_connected = false;
	serialPort = new QextSerialPort(QextSerialPort::EventDriven, this);
	serialPort->setBaudRate(BAUD115200);
	serialPort->setDataBits(DATA_8);
	connect(serialPort, SIGNAL(readyRead()), SLOT(readSerialFunction()));
	m_timer.setSingleShot(true);
	m_timer.setInterval(2500);
	connect(&m_timer, SIGNAL(timeout()), SLOT(sendConnectSignal()));
	
	changeStatus(Object::Error);
	changeInfos(tr("No connection."));
}

Arduino::~Arduino()
{
	serialPort->close();
	delete serialPort;
}
	
void Arduino::more()
{
	QList<QextPortInfo> tempInfos = QextSerialEnumerator::getPorts();
	QStringList tempList;
	for (int i=0 ; i < tempInfos.size() ; i++) tempList << tempInfos.at(i).portName;
	
	int current = tempList.indexOf(serialPort->portName());
	if (current == -1) current = 0;
	
	bool Ok = false;
	QString tempPortName = QInputDialog::getItem(project(), tr("Choose a serial port..."), tr("Select the serial port which correspond to %1:").arg(name()), tempList, current, false, &Ok);
	
	if (Ok)
	{
		tryConnection(tempPortName);
	}
}
	
QVariant Arduino::settings()
{
	return QVariant(serialPort->portName());
}

void Arduino::setSettings(QVariant Settings)
{
	tryConnection(Settings.toString());
}

bool Arduino::isConnected() const
{
	return m_connected;
}
	
void Arduino::prepareFunction(char Code)
{
	if (!m_connected) return;
	arrayToSend.clear();
	arrayToSend.append(Code);
}

void Arduino::prepareBool(bool Value)
{
	if (!m_connected) return;
	if (Value) arrayToSend.append((char)0x01);
	else arrayToSend.append((char)0x00);
}

void Arduino::prepareInt(int Value)
{
	if (!m_connected) return;
	qint16 tValue = Value;
	qint16 tFirstValue = tValue;
	qint16 tSecondValue = tValue;
	
	char firstByte = tFirstValue >> 8;
	char secondByte = tSecondValue & 0xFF;
	
	arrayToSend.append(firstByte);
	arrayToSend.append(secondByte);
}

void Arduino::prepareUInt(unsigned int Value)
{
	if (!m_connected) return;
	quint16 tValue = Value;
	quint16 tFirstValue = tValue;
	quint16 tSecondValue = tValue;
	
	char firstByte = tFirstValue >> 8;
	char secondByte = tSecondValue & 0xFF;
	
	arrayToSend.append(firstByte);
	arrayToSend.append(secondByte);
}

void Arduino::send()
{
	if (!m_connected) return;
	serialPort->write(arrayToSend);
	QString tempLogMessage("Message send:");
	for (int i=0 ; i < arrayToSend.size() ; i++) tempLogMessage += " 0x" + QString::number(int(arrayToSend.at(i)) & 0xFF, 16);
	project()->log()->write(name(), tempLogMessage);
}
	
bool Arduino::takeBool()
{
	QByteArray tempValue = serialPort->read(1);
	if (tempValue.size() != 1) return false;
	
	if (tempValue.at(0) == 0) return false;
	else return true;
}

int Arduino::takeInt()
{
	QByteArray tempValue = serialPort->read(2);
	if (tempValue.size() != 2) return 0;
	
	qint16 tempInt = 0;
	tempInt = ((qint16)tempValue.at(0) & 0xFF) << 8;
	tempInt = tempInt | ((qint16)tempValue.at(1) & 0xFF);
	return tempInt;
}

unsigned int Arduino::takeUInt()
{
	QByteArray tempValue = serialPort->read(2);
	if (tempValue.size() != 2) return 0;
	
	quint16 tempInt = 0;
	tempInt = ((quint16)tempValue.at(0) & 0xFF) << 8;
	tempInt = tempInt | ((quint16)tempValue.at(1) & 0xFF);
	return tempInt;
}

void Arduino::tryConnection(QString Name)
{
	m_connected = false;
	serialPort->close();
	serialPort->setPortName(Name);
	if (!serialPort->open(QIODevice::ReadWrite))
	{
		project()->log()->write(name(), "Connection to \"" + Name + "\" impossible.");
		changeStatus(Object::Error);
		changeInfos(tr("Connection to \"%1\" impossible.").arg(Name));
		serialPort->setPortName(QString());
	}
	else
	{
		m_timer.start();
		changeStatus(Object::Unknown);
		changeInfos(tr("Try connection to \"%1\"...").arg(Name));
	}
}

void Arduino::connectionSuccess()
{
	m_connected = true;
	project()->log()->write(name(), "Connected to \"" + serialPort->portName() + "\".");
	changeStatus(Object::Operational);
	changeInfos(tr("Connected to \"%1\".").arg(serialPort->portName()));
}

void Arduino::sendConnectSignal()
{
	serialPort->write(QByteArray(1, 0xFF));
}

void Arduino::readSerialFunction()
{
	project()->log()->write(name(), "Data received, content: " + QString(serialPort->peek(serialPort->bytesAvailable()).toHex()) + " (" + QString::number(serialPort->bytesAvailable()) + " bytes)");
	if (isConnected())
		{
		bool codeFind = false;
		do
			{
			QByteArray tempValue = serialPort->peek(1);
			if (tempValue.size() == 1)
				{
				char tempCode = tempValue.at(0);
				codeFind = true;
		
				signalReceived(tempCode);
				}
			else codeFind = false;
			}
		while (codeFind);
		}
	else
		{
		QByteArray tempValue = serialPort->peek(identifier().size());
		
		if (tempValue == identifier().toLatin1()) {
			serialPort->read(identifier().size());
			connectionSuccess();
			}
		}
}


