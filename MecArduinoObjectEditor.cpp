/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecArduinoObjectEditor.h"

MecArduinoObjectEditor::MecArduinoObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
pushButtonArduinoCode = new QPushButton(tr("Copy Arduino code"), this);
	connect(pushButtonArduinoCode, SIGNAL(clicked(bool)), SLOT(copyArduinoCode()));

tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));

layoutGeneral->addWidget(pushButtonArduinoCode);
}

MecArduinoObjectEditor::~MecArduinoObjectEditor()
{
}
	
bool MecArduinoObjectEditor::canAddFunction() const
{
	int count=0;
	for (int i=0 ; i < element()->childElements().size() ; i++) count++;
	//Pour les fonctions « identifier » et « isConnected ».
	count -= 2;
	
	if (count <= 254) return true;
	else return false;
}

bool MecArduinoObjectEditor::canAddSignal() const
{
	int count=0;
	for (int i=0 ; i < element()->childElements().size() ; i++) count++;
	//Pour les fonctions « identifier » et « isConnected ».
	count -= 2;
	
	if (count <= 254) return true;
	else return false;
}

bool MecArduinoObjectEditor::canAddVariable() const
{
	return false;
}

bool MecArduinoObjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
	if (Element->elementName() == "identifier" or Element->elementName() == "isConnected") return false;
	else return true;
}

MecAbstractElementEditor* MecArduinoObjectEditor::newSubEditor(MecAbstractElement *Element)
{
	if (Element->elementRole() == MecAbstractElement::Function)
		{
		MecElementEditor *tempEditor = 0;
		if (Element->elementName() == "identifier" or Element->elementName() == "isConnected") tempEditor = new MecArduinoSpecialFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
		else tempEditor = new MecArduinoFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
		tempEditor->childListElementsChanged(Element);
		tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
		connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
		return tempEditor;
		}
	else if (Element->elementRole() == MecAbstractElement::Signal)
		{
		MecElementEditor *tempEditor = new MecArduinoSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
		tempEditor->childListElementsChanged(Element);
		tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
		connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
		return tempEditor;
		}
else return 0;
}
	
void MecArduinoObjectEditor::addFunction()
{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Function, "void");
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the variable “%1”").arg(tempElement->elementName()));
}
	
void MecArduinoObjectEditor::addSignal()
{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Signal, "void");
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
}
	
void MecArduinoObjectEditor::addVariable()
{
return;
}

void MecArduinoObjectEditor::copyArduinoCode()
{
QGuiApplication::clipboard()->setText(arduinoCode());
}

QString MecArduinoObjectEditor::arduinoCode()
{
QString tempString("/* \n\
 * This namespace contains all the functions about the Mécanique process.\n\
 * You can include this code directly into the main file of your program, or write it in another file and include it like this:\n\
 * \n\
 * #include \"MyFile.h\" \n\
 *\n\
 * Note: Do not touch other functions than \"loop()\" unless you know what you do.\n\
 */\n\
\n\
#include <Arduino.h>\n\
\n\
namespace Mec {\n\n");

tempString += "\
  /* You can find here the declaration of the different functions and signals which has been created into the Mécanique project.\n\
   * \n\
   * The functions must be implemented in the program body like a normal function:\n\
   * \n\
   * void Mec::MyFunction(unsigned int MyParameter1, boolean MyParameter2)\n\
   * {\n\
   *   \n\
   * }\n\
   * \n\
   * and the signals have just to be called to be transmitted to the Mécanique project:\n\
   * \n\
   * //...code...\n\
   * Mec::MySignal(Variable1, Variable2);\n\
   * //...code...\n\
   * \n\
   */\n";

//On établi la liste par ordre alphabétique des éléments enfants.
QList<MecAbstractElement*> tempListElements;
QStringList tempElementsNames;
for (int i=0 ; i < element()->childElements().size() ; i++)
	{
	if (element()->childElements().at(i)->elementName() != "identifier" and element()->childElements().at(i)->elementName() != "isConnected")
		tempElementsNames.append(element()->childElements().at(i)->elementName());
	}
tempElementsNames.sort();

for (int i=0 ; i < tempElementsNames.size() ; i++)
	{
	for (int j=0 ; j < element()->childElements().size() ; j++)
		{
		if (tempElementsNames.at(i) == element()->childElements().at(j)->elementName())
			tempListElements.append(element()->childElements().at(j));
		}
	}

//Et l'identifiant correspondant.
QString tempIdentifier;
QString typesString;
for (int i=0 ; i < tempListElements.size() ; i++)
	{
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		typesString += tempListElements.at(i)->childElements().at(j)->elementType();
		}
	}
tempIdentifier = element()->elementName() + QString(QCryptographicHash::hash(typesString.toUtf8(), QCryptographicHash::Md4).toHex());

//On déclare chaque fonction ou signal.
for (int i=0 ; i < element()->childElements().size() ; i++)
	{
	if (element()->childElements().at(i)->elementName() == "identifier" or element()->childElements().at(i)->elementName() == "isConnected") continue;
	
	tempString += "  void " + element()->childElements().at(i)->elementName() + "(";
	//Variables-paramètres
	for (int j=0 ; j < element()->childElements().at(i)->childElements().size() ; j++)
		{
		if (element()->childElements().at(i)->childElements().at(j)->elementType() == "bool")
			{
			tempString += "boolean ";
			}
		else if (element()->childElements().at(i)->childElements().at(j)->elementType() == "int")
			{
			tempString += "int ";
			}
		else if (element()->childElements().at(i)->childElements().at(j)->elementType() == "uint")
			{
			tempString += "unsigned int ";
			}
		tempString += element()->childElements().at(i)->childElements().at(j)->elementName();
		if (j != element()->childElements().at(i)->childElements().size() - 1) tempString += ", ";
		}
	
	tempString += ");\n";
	}

tempString += "\n  //\"actualLoop\" refers to a loop procedure.\n  void (*actualLoop) (void) = 0;\n\
\n\
  //Specific functions for Mécanique.\n\
  bool takeBool();\n\
  int takeInt();\n\
  unsigned int takeUInt();\n\
  void writeBool(byte Buffer[], unsigned int Position, boolean Value);\n\
  void writeInt(byte Buffer[], unsigned int Position, int Value);\n\
  void writeUInt(byte Buffer[], unsigned int Position, unsigned int Value);\n\
\n\
\n\
  /* Call this function in the global \"setup\" function like this:\n\
   *\n\
   * void setup()\n\
   * {\n\
   *   Mec::setup();\n\
   *   //Your code...\n\
   * }\n\
   */\n\
  void setup()\n\
  {\n\
    //Do not change the baud rate.\n\
    Serial.begin(115200);\n\
  }\n\
\n\
  /* Call this function in the global \"loop\" function like this:\n\
   *\n\
   * void loop()\n\
   * {\n\
   *   Mec::loop();\n\
   *   //Your code...\n\
   * }\n\
   */\n\
  void loop()\n\
  {\n\
    if (Serial.available())\n\
    {\n\
      byte mecCode = Serial.peek();\n\
      int mecAvailable = Serial.available();\n\
      \n\
      /* When proposed, you can choose between a simple call or a loop call for the functions without parameter.\n\
       * You just have to comment or not the corresponding line between \"MyFunction();\" and \"actualLoop = MyFunction;\"\n\
       */\n\
      if (mecCode == 0xFF && mecAvailable >= 1)\n\
      {\n\
        Serial.read();\n\
        Serial.print(F(\"" + tempIdentifier + "\"));\n\
      }\n";

for (int i=0 ; i < tempListElements.size() ; i++)
	{
	//On ne traite que les fonctions.
	if (tempListElements.at(i)->elementRole() != MecAbstractElement::Function) continue;
	
	//Compte du nombre d'octets necéssaires.
	int nbBytes = 1;//"1" pour le code de signal.
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		//Préparation de chaque variable.
			if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool") nbBytes += 1;
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int") nbBytes += 2;
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint") nbBytes += 2;
		}

	tempString += "      else if (mecCode == 0x" + QString::number(i, 16) + " && mecAvailable >= " + QString::number(nbBytes) + ")\n      {\n";
	if (tempListElements.at(i)->childElements().size() == 0)
		{
		tempString += "        Serial.read();\n        //" + tempListElements.at(i)->elementName() + "();\n        //or\n        actualLoop = " + tempListElements.at(i)->elementName() + ";\n";
		}
	else
		{
		tempString += "        Serial.read();\n";
		for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
			{
			//Préparation de chaque variable.
			if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool") tempString += "        bool variable" + QString::number(j) + " = takeBool();\n";
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int") tempString += "        int variable" + QString::number(j) + " = takeInt();\n";
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint") tempString += "        unsigned int variable" + QString::number(j) + " = takeUInt();\n";
			
			}
		
		//Appel de la fonction.
		tempString += "        " + tempListElements.at(i)->elementName() + "(";
		//Variables-paramètres
		for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
			{
			tempString += "variable" + QString::number(j);
			if (j != tempListElements.at(i)->childElements().size() - 1) tempString += ", ";
			}
		
		tempString += ");\n";
		}
	tempString += "      }\n";
	}

tempString += "    }\n    if (actualLoop != 0) actualLoop();\n  }\n\n";

for (int i=0 ; i < tempListElements.size() ; i++)
	{
	//On ne traite que les signaux.
	if (tempListElements.at(i)->elementRole() != MecAbstractElement::Signal) continue;
	
	tempString += "  void " + tempListElements.at(i)->elementName() + "(";
	//Variables-paramètres
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool")
			{
			tempString += "boolean ";
			}
		else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int")
			{
			tempString += "int ";
			}
		else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint")
			{
			tempString += "unsigned int ";
			}
		tempString += tempListElements.at(i)->childElements().at(j)->elementName();
		if (j != tempListElements.at(i)->childElements().size() - 1) tempString += ", ";
		}
	tempString += ")\n  {\n";
	
	//Compte du nombre d'octets necéssaires.
	int nbBytes = 1;//"1" pour le code de signal.
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		//Préparation de chaque variable.
			if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool") nbBytes += 1;
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int") nbBytes += 2;
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint") nbBytes += 2;
		}
	
	tempString += "    byte buffer[" + QString::number(nbBytes) + "] = {0x" + QString::number(i, 16) + "};\n";
	
	int nbWritten = 1;
	for (int j=0 ; j < tempListElements.at(i)->childElements().size() ; j++)
		{
		//Préparation de chaque variable.
			if (tempListElements.at(i)->childElements().at(j)->elementType() == "bool")
				{
				tempString += "    writeBool(buffer, " + QString::number(nbWritten) + ", " + tempListElements.at(i)->childElements().at(j)->elementName() + ");\n";
				nbWritten += 1;
				}
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "int")
				{
				tempString += "    writeInt(buffer, " + QString::number(nbWritten) + ", " + tempListElements.at(i)->childElements().at(j)->elementName() + ");\n";
				nbWritten += 2;
				}
			else if (tempListElements.at(i)->childElements().at(j)->elementType() == "uint")
				{
				tempString += "    writeUInt(buffer, " + QString::number(nbWritten) + ", " + tempListElements.at(i)->childElements().at(j)->elementName() + ");\n";
				nbWritten += 2;
				}
		}
	
	tempString += "    Serial.write(buffer, " + QString::number(nbBytes) + ");\n  }\n\n";
	}

tempString += "\
  bool takeBool()\n\
  {\n\
    if (Serial.read() == 0) return false;\n\
    else return true;\n\
  }\n\
\n\
  int takeInt()\n\
  {\n\
    int number = (Serial.read() & 0xFF) << 8;\n\
    number = number | (Serial.read() & 0xFF);\n\
    return number;\n\
  }\n\
\n\
  unsigned int takeUInt()\n\
  {\n\
    unsigned int number = (Serial.read() & 0xFF) << 8;\n\
    number = number | (Serial.read() & 0xFF);\n\
    return number;\n\
  }\n\
\n\
  void writeBool(byte Buffer[], unsigned int Position, boolean Value)\n\
  {\n\
    if (Value) Buffer[Position] = 1;\n\
    else Buffer[Position] = 0;\n\
  }\n\
\n\
  void writeInt(byte Buffer[], unsigned int Position, int Value)\n\
  {\n\
    int ValueCpy = Value;\n\
    Buffer[Position] = Value >> 8;\n\
    Buffer[Position+1] = ValueCpy & 0xFF;\n\
  }\n\
\n\
  void writeUInt(byte Buffer[], unsigned int Position, unsigned int Value)\n\
  {\n\
    unsigned int ValueCpy = Value;\n\
    Buffer[Position] = Value >> 8;\n\
    Buffer[Position+1] = ValueCpy & 0xFF;\n\
  }\n\
";

tempString += "}\n\n";//Clôture du namespace

return tempString;
}


