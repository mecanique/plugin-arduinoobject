######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = arduinoobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/arduinoobject.fr.ts

OTHER_FILES += metadata.json

HEADERS += \
	MecArduinoFunctionEditor.h \
	MecArduinoSpecialFunctionEditor.h \
	MecArduinoObjectCompiler.h \
	MecArduinoObjectEditor.h \
	MecArduinoObjectPlugin.h \
	MecArduinoSignalEditor.h  \
	MecArduinoVariableEditor.h


SOURCES += \
	MecArduinoFunctionEditor.cpp \
	MecArduinoSpecialFunctionEditor.cpp \
	MecArduinoObjectCompiler.cpp \
	MecArduinoObjectEditor.cpp \
	MecArduinoObjectPlugin.cpp \
	MecArduinoSignalEditor.cpp \
	MecArduinoVariableEditor.cpp


