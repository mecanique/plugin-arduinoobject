/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECARDUINOOBJECTEDITOR_H__
#define __MECARDUINOOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include <QCryptographicHash>
#include "MecArduinoFunctionEditor.h"
#include "MecArduinoSpecialFunctionEditor.h"
#include "MecArduinoSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « Arduino ».

Les objets de type « Arduino » ne peuvent avoir plus de 254 fonctions et signaux, ceci en raison du protocole de communication utilisé.
*/
class MecArduinoObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecArduinoObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecArduinoObjectEditor();

	/**
	\brief	Indique si une MecFunction peut être ajouté.
	\return	true si l'objet a strictement moins de 254 fonctions et signaux (fonctions « identifier » et « isConnected » non comptées), sinon false.
	*/
	bool canAddFunction() const;
	/**
	\brief	Indique si un MecSignal peut être ajouté.
	\return	true si l'objet a strictement moins de 254 fonctions et signaux (fonctions « identifier » et « isConnected » non comptées), sinon false.
	*/
	bool canAddSignal() const;
	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e true sauf s'il s'agit des fonctions « identifier » ou « isConnected ».
	bool canRemoveChild(MecAbstractElement* const Element) const;

public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element(), et être une fonction ou un signal, sinon 0 est retourné.
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Ajoute une fonction à l'objet Arduino.

	La fonction sera soumise aux contraintes des types supportés par le protocole de communication, son type de retour sera nécessairement « void ».
	*/
	void addFunction();

	/**
	\brief	Ajoute un signal à l'objet Arduino.

	Le signal sera soumis aux contraintes des types supportés par le protocole de communication.
	*/
	void addSignal();

	///Ne fait rien.
	void addVariable();

	/**
	\brief	Place le code à intégrer dans le programme d'une Arduino dans le presse-papiers.
	*/
	void copyArduinoCode();

	/**
	\brief	Retourne le code à intégrer dans le programme d'une Arduino pour communiquer avec elle, selon les caractéristique de l'objet.
	*/
	QString arduinoCode();

signals:


private:
	/**
	\brief	Bouton de copie du code embarqué pour Arduino.

	Est connecté à copyArduinoCode();
	*/
	QPushButton *pushButtonArduinoCode;


};

#endif /* __MECARDUINOOBJECTEDITOR_H__ */

