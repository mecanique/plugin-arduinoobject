/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecArduinoObjectPlugin.h"

MecArduinoObjectPlugin::MecArduinoObjectPlugin() : MecPlugin(QString("arduinoobject"), __ARDUINOOBJECT_VERSION__, MecAbstractElement::Object, QString("Arduino"), QString("Arduino object"))
{
}

MecArduinoObjectPlugin::~MecArduinoObjectPlugin()
{
}

QString MecArduinoObjectPlugin::description() const
{
return QString(tr("Design an Arduino object to include into a Mécanique project."));
}

QString MecArduinoObjectPlugin::copyright() const
{
return QString("Copyright © 2014 – 2015 Quentin VIGNAUD");
}

QString MecArduinoObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecArduinoObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecArduinoObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecArduinoObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecArduinoObjectEditor *tempEditor = new MecArduinoObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecArduinoObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecArduinoObjectCompiler *tempCompiler = new MecArduinoObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}

